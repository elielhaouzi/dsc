%%% dsc_api_SUITE.erl
%%% vim: ts=3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% @copyright 2018 SigScale Global Inc.
%%% @end
%%% Licensed under the Apache License, Version 2.0 (the "License");
%%% you may not use this file except in compliance with the License.
%%% You may obtain a copy of the License at
%%%
%%%     http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing, software
%%% distributed under the License is distributed on an "AS IS" BASIS,
%%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%%% See the License for the specific language governing permissions and
%%% limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  @doc Test suite for public API of the {@link //dsc. dsc} application.
%%%
-module(dsc_api_SUITE).
-copyright('Copyright (c) 2018 SigScale Global Inc.').

%% common_test required callbacks
-export([suite/0, sequences/0, all/0]).
-export([init_per_suite/1, end_per_suite/1]).
-export([init_per_testcase/2, end_per_testcase/2]).

%% Note: This directive should only be used in test suites.
-compile(export_all).

-include("dsc.hrl").
-include_lib("common_test/include/ct.hrl").
-include_lib("diameter/include/diameter.hrl").

%%---------------------------------------------------------------------
%%  Test server callback functions
%%---------------------------------------------------------------------

-spec suite() -> DefaultData :: [tuple()].
%% Require variables and set default values for the suite.
%%
suite() ->
	[{userdata, [{doc, "Test suite for public API in DSC"}]}].

-spec init_per_suite(Config :: [tuple()]) -> Config :: [tuple()].
%% Initialization before the whole suite.
%%
init_per_suite(Config) ->
	Config.

-spec end_per_suite(Config :: [tuple()]) -> any().
%% Cleanup after the whole suite.
%%
end_per_suite(Config) ->
	Config.

-spec init_per_testcase(TestCase :: atom(), Config :: [tuple()]) -> Config :: [tuple()].
%% Initialization before each test case.
%%
init_per_testcase(_TestCase, Config) ->
	case is_alive() of
		true ->
			ok = dsc_test_lib:initialize_db(?config(priv_dir, Config)),
			ok = dsc_test_lib:start(),
			Config;
		false ->
			{skip, not_alive}
	end.

-spec end_per_testcase(TestCase :: atom(), Config :: [tuple()]) -> any().
%% Cleanup after each test case.
%%
end_per_testcase(_TestCase, _Config) ->
	ok = dsc_test_lib:stop(),
	ok.

-spec sequences() -> Sequences :: [{SeqName :: atom(), Testcases :: [atom()]}].
%% Group test cases into a test sequence.
%%
sequences() -> 
	[].

-spec all() -> TestCases :: [Case :: atom()].
%% Returns a list of all test cases in this test suite.
%%
all() -> 
	[add_transport, find_nonexisting_transport, find_existing_transport, delete_transport,
		stop_transport, add_realm, find_nonexisting_realm, find_existing_realm,
		delete_realm].

%%---------------------------------------------------------------------
%%  Test cases
%%---------------------------------------------------------------------

add_transport() ->
	[{userdata, [{doc, "add a transport to dsc_transport table"}]}].

add_transport(_Config) ->
	Name = rand:uniform(100000),
	Transport = random_tranport(),
	Options = random_transport_opts(Transport),
	{ok, T} = dsc:add_transport(Name, Transport, Options),
	#dsc_transport{name = Name, type = Transport, transport_options = Options} = T.

find_nonexisting_transport() ->
	[{userdata, [{doc, "Attempt to find non existing transport in dsc_transport table"}]}].

find_nonexisting_transport(_Config) ->
	Name = rand:uniform(100000),
	{error, not_found} = dsc:find_transport(Name).

find_existing_transport() ->
	[{userdata, [{doc, "Attempt to find existing transport in dsc_transport table"}]}].

find_existing_transport(_Config) ->
	Name = rand:uniform(100000),
	Transport = random_tranport(),
	Options = random_transport_opts(Transport),
	{ok, _} = dsc:add_transport(Name, Transport, Options),
	{ok, Trans} = dsc:find_transport(Name),
	#dsc_transport{name = Name, type = Transport, transport_options = Options} = Trans.

stop_transport() ->
	[{userdata, [{doc, "Stop a stated transport process"}]}].

stop_transport(_Config) ->
	Name = ?DSC_SVC,
	diameter:start_service(Name, [{application, [{dictionary, diameter_gen_relay},{module, dsc_diameter_base_application_cb}]}]),
	Transport = random_tranport(),
	Options = random_transport_opts(Transport),
	{ok, _}  = dsc:add_transport(Name, Transport, Options),
	{ok, Ref} = dsc:start_transport(Name),
	Transports = diameter:service_info(Name, transport),
	true = [] /= Transports,
	ok = dsc:stop_transport(Ref).

delete_transport() ->
	[{userdata, [{doc, "Delete a transport in dsc_transport table"}]}].

delete_transport(_Config) ->
	Name = rand:uniform(100000),
	Transport = random_tranport(),
	Options = random_transport_opts(Transport),
	{ok, _} = dsc:add_transport(Name, Transport, Options),
	{ok, _} = dsc:find_transport(Name),
	ok = dsc:delete_transport(Name),
	{error, not_found} = dsc:find_transport(Name).

add_realm() ->
	[{userdata, [{doc, "add a realm to dsc_realm table"}]}].

add_realm(_Config) ->
	Name = dsc_test_lib:random_str(),
	AppId = rand:uniform(100),
	Mode = dsc_test_lib:realm_mode(),
	Peers = [dsc_test_lib:random_str()],
	{ok, Realm} = dsc:add_realm(Name, AppId, Mode, Peers),
	NameB = list_to_binary(Name),
	#dsc_realm{name = NameB, app_id = AppId, mode = Mode, peers = Peers} = Realm.

find_nonexisting_realm() ->
	[{userdata, [{doc, "Find a non existing realm in dsc_realm table"}]}].

find_nonexisting_realm(_Config) ->
	Name = dsc_test_lib:random_str(),
	{error, not_found} = dsc:find_realm(Name).

find_existing_realm() ->
	[{userdata, [{doc, "Find an existing realm in dsc_realm table"}]}].

find_existing_realm(_Config) ->
	Name = list_to_binary(dsc_test_lib:random_str()),
	AppId = rand:uniform(100),
	Mode = dsc_test_lib:realm_mode(),
	Peers = [dsc_test_lib:random_str()],
	{ok, _} = dsc:add_realm(Name, AppId, Mode, Peers),
	{ok, Realm} = dsc:find_realm(Name),
	#dsc_realm{name = Name, app_id = AppId, mode = Mode, peers = Peers} = Realm.

delete_realm() ->
	[{userdata, [{doc, "Find an existing realm in dsc_realm table"}]}].

delete_realm(_Config) ->
	Name = dsc_test_lib:random_str(),
	AppId = rand:uniform(100),
	Mode = dsc_test_lib:realm_mode(),
	Peers = [dsc_test_lib:random_str()],
	{ok, _} = dsc:add_realm(Name, AppId, Mode, Peers),
	{ok, _} = dsc:find_realm(Name),
	ok = dsc:delete_realm(Name),
	{error, not_found} = dsc:find_realm(Name).

%%---------------------------------------------------------------------
%%  Internal functions
%%---------------------------------------------------------------------

%% @hidden
addr({0,0,0,0}) ->
	{127,0,0,1};
addr(A) ->
	A.

%% @hidden
random_tranport() ->
	case rand:uniform(2) of
		1 ->
			connect;
		_ ->
			listen
	end.

%% @hidden
random_transport_opts(connect) ->
	[{capabilities,
			[{'Origin-Host', dsc_test_lib:random_str()},
			{'Origin-Realm', dsc_test_lib:random_str()},
			{'Product-Name', dsc_test_lib:random_str()},
			{'Vendor-Id', rand:uniform(100)},
			{'Firmware-Revision', rand:uniform(100)}]
		},
	{transport_module, diameter_tcp},
	{transport_config,
			[{raddr, {10,140,0,7}},
			{rport, 3868},
			{reuseaddr, true},
			{ip, {0,0,0,0}}]
	}];
random_transport_opts(listen) ->
	[{capabilities,
			[{'Origin-Host', dsc_test_lib:random_str()},
			{'Origin-Realm', dsc_test_lib:random_str()},
			{'Product-Name', dsc_test_lib:random_str()},
			{'Vendor-Id', rand:uniform(100)},
			{'Firmware-Revision', rand:uniform(100)}]
		},
	{transport_module, diameter_tcp},
	{transport_config,
			[{ip, {0,0,0,0}},
			{port, 9968}]
	}].
