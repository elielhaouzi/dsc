%%% dsc.hrl
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% @copyright 2015-2018 SigScale Global Inc.
%%% @end
%%% Licensed under the Apache License, Version 2.0 (the "License");
%%% you may not use this file except in compliance with the License.
%%% You may obtain a copy of the License at
%%%
%%%     http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing, software
%%% distributed under the License is distributed on an "AS IS" BASIS,
%%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%%% See the License for the specific language governing permissions and
%%% limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%

-define(DSC_SVC, dsc_relay). 

%% Define service table entry record
-record(dsc_transport,
		{name :: term(),
		type :: listen | connect,
		transport_options :: [diameter:transport_opt()]}).

%% Define realm table entry record as described in rfc 6733, section 2.7
-record(dsc_realm,
		{name :: binary(),
		app_id :: non_neg_integer(),
		mode :: local | relay | proxy | redirect,
		peers :: [PeerName :: string()]}).

